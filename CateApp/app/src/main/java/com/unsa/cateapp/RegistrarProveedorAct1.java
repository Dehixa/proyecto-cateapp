package com.unsa.cateapp;

import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrarProveedorAct1 extends AppCompatActivity {

    // Campos editables
    TextInputEditText razonSocial;
    TextInputEditText ruc;
    TextInputEditText email;
    TextInputEditText paginaWeb;
    TextInputEditText telefono;

    // Layouts de los campos editables
    TextInputLayout layout_razonSocial;
    TextInputLayout layout_ruc;
    TextInputLayout layout_email;
    TextInputLayout layout_paginaWeb;
    TextInputLayout layout_telefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_proveedor_act1);

        razonSocial = findViewById(R.id.editTextRazonSocial);
        ruc = findViewById(R.id.editTextRuc);
        email = findViewById(R.id.editTextCorreo);
        paginaWeb = findViewById(R.id.editTextPaginaWeb);
        telefono = findViewById(R.id.editTextTelefono);

        layout_razonSocial = findViewById(R.id.razon_social_text_input);
        layout_ruc = findViewById(R.id.ruc_text_input);
        layout_email = findViewById(R.id.correo_text_input);
        layout_paginaWeb = findViewById(R.id.pagina_web_text_input);
        layout_telefono = findViewById(R.id.telefono_text_input);

        Button btn_sgte1 = findViewById(R.id.btn_Siguiente1);

        btn_sgte1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validarDatos()){
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("razonSocial", razonSocial.getText().toString());
                        jsonObject.put("ruc", ruc.getText().toString());
                        jsonObject.put("email", email.getText().toString());
                        jsonObject.put("paginaWeb", paginaWeb.getText().toString());
                        jsonObject.put("telefono", telefono.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(RegistrarProveedorAct1.this, RegistrarProveedorAct2.class);
                    intent.putExtra("datos1", jsonObject.toString());
                    startActivity(intent);
                }

            }
        });
    }

    // Validacion de campos de resgistro
    private boolean validarDatos() {
        boolean valido = true;

        // Comprueba que el campo de Razon Social no este vacio
        if (TextUtils.isEmpty(razonSocial.getText().toString())) {
            layout_razonSocial.setError("Debe ingresar una Razón Social.");
            layout_razonSocial.setErrorEnabled(true);
            valido = false;
        } else {
            layout_razonSocial.setErrorEnabled(false);
        }

        // Comprueba que el campo de RUC no este vacio
        if (TextUtils.isEmpty(ruc.getText().toString())) {
            layout_ruc.setError("Debe ingresar un número de RUC.");
            layout_ruc.setErrorEnabled(true);
            valido = false;
        } else {
            layout_ruc.setErrorEnabled(false);
        }

        // Comprueba que el campo de Email no este vacio o que contenga un @.
        if (TextUtils.isEmpty(email.getText().toString())) {
            layout_email.setError("Debe ingresar un Email.");
            layout_email.setErrorEnabled(true);
            valido = false;
        } else if (TextUtils.indexOf(email.getText().toString(), "@") == -1){
            layout_email.setError("El Email debe contener un @.");
            layout_email.setErrorEnabled(true);
            valido = false;
        } else {
            layout_email.setErrorEnabled(false);
        }

        // Comprueba que el campo de pagina web no este vacio.
        if (TextUtils.isEmpty(paginaWeb.getText().toString())) {
            layout_paginaWeb.setError("Debe ingresar una Página Web.");
            layout_paginaWeb.setErrorEnabled(true);
            valido = false;
        } else {
            layout_paginaWeb.setErrorEnabled(false);
        }

        // Comprueba que el campo de Telefono no este vacio.
        if (TextUtils.isEmpty(telefono.getText().toString())) {
            layout_telefono.setError("Debe ingresar un Número telefónico.");
            layout_telefono.setErrorEnabled(true);
            valido = false;
        } else {
            layout_telefono.setErrorEnabled(false);
        }

        return valido;
    }
}