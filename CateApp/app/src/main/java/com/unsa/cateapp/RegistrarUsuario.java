package com.unsa.cateapp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.unsa.cateapp.ConexionRed.AsyncTaskListener;
import com.unsa.cateapp.ConexionRed.HostURL;
import com.unsa.cateapp.ConexionRed.HttpTask;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;

public class RegistrarUsuario extends AppCompatActivity implements View.OnClickListener{

    // Campos editables
    TextInputEditText nombres;
    TextInputEditText apePaterno;
    TextInputEditText apeMaterno;
    TextInputEditText password;
    TextInputEditText email;
    TextInputEditText telefono;
    TextInputEditText fecha;

    // Layouts de los campos editables
    TextInputLayout layout_nombres;
    TextInputLayout layout_apePat;
    TextInputLayout layout_apeMat;
    TextInputLayout layout_password;
    TextInputLayout layout_email;
    TextInputLayout layout_telefono;
    TextInputLayout layout_fecha;

    String datos1;
    String datos2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        nombres = findViewById(R.id.editTextNombresPropietario);
        apePaterno = findViewById(R.id.editTextApePatPropietario);
        apeMaterno = findViewById(R.id.editTextApeMatPropietario);
        password = findViewById(R.id.editTextPassword);
        email = findViewById(R.id.editTextCorreo);
        telefono = findViewById(R.id.editTextTelefono);
        fecha = findViewById(R.id.editTextFecha);

        layout_nombres = findViewById(R.id.nombres_propietario_text_input);
        layout_apePat = findViewById(R.id.apePat_propietario_text_input);
        layout_apeMat = findViewById(R.id.apeMat_propietario_text_input);
        layout_password = findViewById(R.id.password_text_input);
        layout_email = findViewById(R.id.correo_text_input);
        layout_telefono = findViewById(R.id.telefono_text_input);
        layout_fecha= findViewById(R.id.fecha_text_input);

        Button btn_registrar = findViewById(R.id.btn_Registrar_Cliente);

        //  datos1 = getIntent().getExtras().getString("datos1");
        //   datos2 = getIntent().getExtras().getString("datos2");

       btn_registrar.setOnClickListener(this);
       fecha.setOnClickListener(this);
    }

    // Validacion de campos de resgistro
    private boolean validarDatos() {
        boolean valido = true;

        // Comprueba que el campo de Nombres no este vacio.
        if (TextUtils.isEmpty(nombres.getText().toString())) {
            layout_nombres.setError("Debe ingresar sus Nombres.");
            layout_nombres.setErrorEnabled(true);
            valido = false;
        } else {
            layout_nombres.setErrorEnabled(false);
        }

        // Comprueba que el campo de apellido paterno no este vacio.
        if (TextUtils.isEmpty(apePaterno.getText().toString())) {
            layout_apePat.setError("Debe ingresar su Apellido Paterno.");
            layout_apePat.setErrorEnabled(true);
            valido = false;
        } else {
            layout_apePat.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(fecha.getText().toString())) {
            layout_fecha.setError("Debe ingresar su fecha de nacimiento.");
            layout_fecha.setErrorEnabled(true);
            valido = false;
        } else {
            layout_fecha.setErrorEnabled(false);
        }

        // Comprueba que el campo de Nombres no este vacio.
        if (TextUtils.isEmpty(apeMaterno.getText().toString())) {
            layout_apeMat.setError("Debe ingresar su Apellido Materno.");
            layout_apeMat.setErrorEnabled(true);
            valido = false;
        } else {
            layout_apeMat.setErrorEnabled(false);
        }

        // Comprueba que el campo de password no este vacio.
        if (TextUtils.isEmpty(password.getText().toString())) {
            layout_password.setError("Debe ingresar una Contraseña.");
            layout_password.setErrorEnabled(true);
            valido = false;
        } else {
            layout_password.setErrorEnabled(false);
        }

        // Comprueba que el campo de Telefono no este vacio.
        if (TextUtils.isEmpty(telefono.getText().toString())) {
            layout_telefono.setError("Debe ingresar un Número telefónico.");
            layout_telefono.setErrorEnabled(true);
            valido = false;
        } else {
            layout_telefono.setErrorEnabled(false);
        }

        // Comprueba que el campo de Email no este vacio o que contenga un @.
        if (TextUtils.isEmpty(email.getText().toString())) {
            layout_email.setError("Debe ingresar un Email.");
            layout_email.setErrorEnabled(true);
            valido = false;
        } else if (TextUtils.indexOf(email.getText().toString(), "@") == -1){
            layout_email.setError("El Email debe contener un @.");
            layout_email.setErrorEnabled(true);
            valido = false;
        } else {
            layout_email.setErrorEnabled(false);
        }


        return valido;
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate = year + "-" + (month + 1) + "-" + day;
                fecha.setText(selectedDate);
            }
        });
        newFragment.show(RegistrarUsuario.this.getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editTextFecha:
                showDatePickerDialog();
                break;
            case R.id.btn_Registrar_Cliente:
                registrar();
                break;
        }
    }

    public void registrar(){
        if (validarDatos()){
            JSONObject jsonObject = new JSONObject();
            try {


                jsonObject.put("metodo", 2);
                jsonObject.put("email", email.getText().toString());
                jsonObject.put("telefono", telefono.getText().toString());
                jsonObject.put("nombres", nombres.getText().toString());
                jsonObject.put("apePat", apePaterno.getText().toString());
                jsonObject.put("apeMat", apeMaterno.getText().toString());
                jsonObject.put("password", password.getText().toString());
                jsonObject.put("fecha", fecha.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("****************************************+");
            //System.out.println(jsonObject.toString());
            Log.d("tag", jsonObject.toString());
            System.out.println("+***************************************+");

            new HttpTask(new AsyncTaskListener() {
                @Override
                public void onHttpTask(String data) {
                    System.out.println("******************************************");
                    System.out.println(data);

                    try {
                        JSONObject respuesta = new JSONObject(data);

                        Toast.makeText(RegistrarUsuario.this, respuesta.getString("message"), Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).execute(HostURL.USUARIO, jsonObject.toString());
        }

    }
}