package com.unsa.cateapp.ConexionRed;

/* Interfaz del Listener personalizado*/

public interface AsyncTaskListener {
    public void onHttpTask(String data);
}
